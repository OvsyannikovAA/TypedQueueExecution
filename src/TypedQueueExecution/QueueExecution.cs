﻿using System;
using System.Collections.Generic;
using TypedQueueExecution.Core.Abstractions;

namespace TypedQueueExecution
{
    public class QueueExecution<T> : IDisposable
    {
        #region Поля

        private readonly SortedDictionary<int, APerformer<T>> _dictionary;

        #endregion

        #region Конструкторы

        public QueueExecution()
        {
            _dictionary = new SortedDictionary<int, APerformer<T>>();
        }

        #endregion

        #region Методы

        public void AddPerformer(int type, APerformer<T> performer)
        {
            _dictionary.Add(type, performer);
        }

        public bool RemovePerformer(int type)
        {
            _dictionary[type].Dispose();
            return _dictionary.Remove(type);
        }

        public bool AddInPerformer(int type, T value)
        {
            if (_dictionary.ContainsKey(type))
                return _dictionary[type].Add(value);
            
            throw new ArgumentOutOfRangeException("Such a key does not exist");
        }

        public APerformer<T> GetPerformer(int type)
        {
            if (!_dictionary.ContainsKey(type))
                throw new ArgumentOutOfRangeException("Such a key does not exist");

            return _dictionary[type];
        }

        public void Dispose()
        {
            foreach (var key in _dictionary.Keys)
            {
                _dictionary[key].Dispose();
            }
        }

        #endregion

        #region Свойства

        #endregion
    }
}