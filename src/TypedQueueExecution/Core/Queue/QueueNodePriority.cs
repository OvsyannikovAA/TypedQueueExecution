﻿using System.Collections.Generic;
using TypedQueueExecution.Core.Abstractions;
using TypedQueueExecution.Core.Abstractions.Queue;
using TypedQueueExecution.Core.Enums;

namespace TypedQueueExecution.Core.Queue
{
    public class QueueNodePriority<T> : IQueueNodes<T> where T : IPriority
    {
        #region Поля

        private readonly SortedDictionary<EPriority, Queue<T>> _dictionary;

        #endregion

        #region Конструкторы

        public QueueNodePriority()
        {
            _dictionary = new SortedDictionary<EPriority, Queue<T>>
            {
                {EPriority.High, new Queue<T>()},
                {EPriority.Normal, new Queue<T>()},
                {EPriority.Low, new Queue<T>()}
            };
        }

        #endregion

        #region Методы

        public void Add(T value)
        {
            _dictionary[value.Priority].Enqueue(value);
        }

        public List<T> ToList()
        {
            var result = new List<T>();

            result.AddRange(_dictionary[EPriority.High].ToArray());
            result.AddRange(_dictionary[EPriority.Normal].ToArray());
            result.AddRange(_dictionary[EPriority.Low].ToArray());

            return result;
        }

        public void Clear()
        {
            _dictionary[EPriority.High].Clear();
            _dictionary[EPriority.Normal].Clear();
            _dictionary[EPriority.Low].Clear();
        }

        #endregion

        #region Свойства

        public T GetNext
        {
            get
            {
                if (_dictionary[EPriority.High].Count > 0)
                    return _dictionary[EPriority.High].Dequeue();

                if (_dictionary[EPriority.Normal].Count > 0)
                    return _dictionary[EPriority.Normal].Dequeue();

                return _dictionary[EPriority.Low].Count > 0 ? _dictionary[EPriority.Low].Dequeue() : default(T);
            }
        }

        public int Count =>
            _dictionary[EPriority.High].Count +
            _dictionary[EPriority.Normal].Count +
            _dictionary[EPriority.Low].Count;

        #endregion
    }
}