﻿using System.Collections.Generic;
using System.Linq;
using TypedQueueExecution.Core.Abstractions.Queue;

namespace TypedQueueExecution.Core.Queue
{
    public class QueueNode<T> : IQueueNodes<T>
    {
        #region Поля

        private readonly Queue<T> _queue;

        #endregion

        #region Конструкторы

        public QueueNode()
        {
            _queue = new Queue<T>();
        }

        #endregion

        #region Методы

        public void Add(T value)
        {
            _queue.Enqueue(value);
        }

        public List<T> ToList()
        {
            return _queue.ToList();
        }

        public void Clear()
        {
            _queue.Clear();
        }

        #endregion

        #region Свойства

        public T GetNext => _queue.Dequeue();
        public int Count => _queue.Count;

        #endregion
    }
}