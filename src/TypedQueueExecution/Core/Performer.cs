﻿using System;
using System.Threading;
using TypedQueueExecution.Core.Abstractions;
using TypedQueueExecution.Core.Abstractions.Queue;

namespace TypedQueueExecution.Core
{
    public class Performer<T> : APerformer<T> where T : IHandler
    {
        #region Поля

        protected readonly object Sync = new object();

        private bool _isWork;
        private readonly ManualResetEvent _event;
        private readonly Thread _thread;

        private readonly IQueueNodes<T> _queue;

        private int _maxLength;

        #endregion

        #region Конструкторы

        public Performer(int maxLength, IQueueNodes<T> queue)
        {
            if (maxLength <= 0)
                throw new InvalidOperationException("Size cannot be smaller than 1");

            _maxLength = maxLength;

            _queue = queue;

            _isWork = true;
            _event = new ManualResetEvent(false);
            _thread = new Thread(Handler)
            {
                IsBackground = true
            };

            _thread.Start();
        }

        #endregion

        #region Методы

        public override bool Add(T handler)
        {
            lock (Sync)
            {
                if (_queue.Count + 1 > _maxLength)
                    return false;

                _queue.Add(handler);

                if (_queue.Count == 1)
                    _event.Set();

                return true;
            }
        }

        protected override void Handler()
        {
            loop:

            Monitor.Enter(Sync);
            {
                if (_queue.Count > 0)
                {
                    var handler = _queue.GetNext;

                    Monitor.Exit(Sync);

                    handler.Handler();
                }
                else
                {
                    Monitor.Exit(Sync);

                    _event.WaitOne();
                    _event.Reset();
                }
            }

            lock (Sync)
            {
                if (_isWork)
                    goto loop;
            }
        }

        public override void Stop()
        {
            Dispose();
        }

        public override void Dispose()
        {
            lock (Sync)
            {
                var result = _queue.ToList();

                _isWork = false;
                _queue.Clear();
                _event.Set();

                OnStop?.Invoke(result);
            }
        }

        #endregion

        #region Свойства

        public override bool IsWork
        {
            get
            {
                lock (Sync)
                {
                    return _isWork;
                }
            }
        }

        public override int MaxLength
        {
            get => _maxLength;
            set
            {
                lock (Sync)
                {
                    if (value <= 0)
                        throw new InvalidOperationException("Size cannot be smaller than 1");

                    _maxLength = value;
                }
            }
        }

        #endregion
    }
}