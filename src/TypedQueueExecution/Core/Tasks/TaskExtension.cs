﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TypedQueueExecution.Core.Tasks
{
    public static class TaskExtension
    {
        #region Методы

        public static void RunSequential(this Task task, params Func<Task>[] actions)
        {
            TaskSequential.RunSequential((Action) null, null, actions.AsEnumerable().GetEnumerator());
        }

        public static void RunSequential(this Task task, Action<Exception> error, params Func<Task>[] actions)
        {
            TaskSequential.RunSequential(null, error, actions.AsEnumerable().GetEnumerator());
        }

        public static void RunSequential(this Task task, Action end, Action<Exception> error,
            params Func<Task>[] actions)
        {
            TaskSequential.RunSequential(end, error, actions.AsEnumerable().GetEnumerator());
        }

        public static void RunSequential(this Task task, Action end, Action<Exception> error,
            IEnumerator<Func<Task>> actions)
        {
            TaskSequential.RunSequential(end, error, actions);
        }

        #endregion
    }
}