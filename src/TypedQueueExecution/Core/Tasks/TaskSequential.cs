﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TypedQueueExecution.Core.Tasks
{
    public class TaskSequential
    {
        #region Методы

        public static void RunSequential(Action end, Action<Exception> error, params Func<Task>[] actions)
        {
            RunSequential(end, error, actions.AsEnumerable().GetEnumerator());
        }

        public static void RunSequential(Action end, Action<Exception> error, IEnumerator<Func<Task>> actions)
        {
            if (actions.MoveNext())
            {
                var taskCurrent = actions.Current();
                taskCurrent.ContinueWith(t => error?.Invoke(t.Exception),
                    TaskContinuationOptions.OnlyOnFaulted);
                taskCurrent.ContinueWith(t => RunSequential(end, error, actions),
                    TaskContinuationOptions.OnlyOnRanToCompletion);
            }
            else
            {
                end?.Invoke();
            }
        }

        #endregion
    }
}