﻿namespace TypedQueueExecution.Core.Abstractions
{
    public interface IHandler
    {
        void Handler();
    }
}