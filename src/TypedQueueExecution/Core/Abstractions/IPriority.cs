﻿using TypedQueueExecution.Core.Enums;

namespace TypedQueueExecution.Core.Abstractions
{
    public interface IPriority
    {
        EPriority Priority { get; }
    }
}