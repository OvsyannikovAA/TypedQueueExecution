﻿using System.Collections.Generic;

namespace TypedQueueExecution.Core.Abstractions.Queue
{
    public interface IQueueNodes<T>
    {
        void Add(T value);
        void Clear();
        List<T> ToList();

        T GetNext { get; }
        int Count { get; }
    }
}