﻿using System;
using System.Collections.Generic;

namespace TypedQueueExecution.Core.Abstractions
{
    public abstract class APerformer<T> : IDisposable
    {
        public Action<List<T>> OnStop;

        public abstract bool Add(T handler);
        public abstract void Stop();
        public abstract void Dispose();

        public abstract bool IsWork { get; }
        public abstract int MaxLength { get; set; }

        protected abstract void Handler();
    }
}