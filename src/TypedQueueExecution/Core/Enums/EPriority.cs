﻿namespace TypedQueueExecution.Core.Enums
{
    public enum EPriority : byte
    {
        High,
        Normal,
        Low
    }
}