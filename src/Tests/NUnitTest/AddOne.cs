﻿using System.Threading;
using NUnit.Framework;
using NUnitTest.Core;
using TypedQueueExecution;
using TypedQueueExecution.Core;
using TypedQueueExecution.Core.Queue;

namespace NUnitTest
{
    [TestFixture]
    public class AddOne
    {
        #region Поля

        private QueueExecution<Node> _queue;
        private ManualResetEvent _event;

        private int _value;
        private int _result;

        #endregion

        #region Методы

        [SetUp]
        public void Setup()
        {
            _queue = new QueueExecution<Node>();
            _event = new ManualResetEvent(false);

            _value = 1;
            _result = 0;
        }

        [Test]
        [Order(0)]
        public void Check()
        {
            _queue.AddPerformer(0, new Performer<Node>(1, new QueueNode<Node>()));
            _queue.AddInPerformer(0, new Node(_value, Action));

            _event.WaitOne(1000);

            Assert.AreEqual(_result, _value);
        }

        private void Action(int value)
        {
            _result = value;
        }

        #endregion
    }
}