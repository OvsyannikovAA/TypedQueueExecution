﻿using System;
using TypedQueueExecution.Core.Abstractions;

namespace NUnitTest.Core
{
    public class Node : IHandler
    {
        #region Поля

        private readonly int _value;
        private readonly Action<int> _action;

        #endregion

        #region Конструкторы

        public Node(int value, Action<int> action)
        {
            _value = value;
            _action = action;
        }

        #endregion

        #region Методы

        public void Handler()
        {
            _action(_value);
        }

        #endregion
    }
}