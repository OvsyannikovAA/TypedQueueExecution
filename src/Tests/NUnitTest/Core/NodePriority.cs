﻿using System;
using TypedQueueExecution.Core.Abstractions;
using TypedQueueExecution.Core.Enums;

namespace NUnitTest.Core
{
    public class NodePriority : IHandler, IPriority
    {
        #region Поля

        private readonly int _value;
        private readonly Action<int> _action;

        #endregion

        #region Конструкторы

        public NodePriority(EPriority priority, int value, Action<int> action)
        {
            Priority = priority;
            _value = value;
            _action = action;
        }

        #endregion

        #region Методы

        public void Handler()
        {
            _action(_value);
        }

        #endregion

        public EPriority Priority { get; }
    }
}