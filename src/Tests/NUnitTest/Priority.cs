﻿using System.Threading;
using NUnit.Framework;
using NUnitTest.Core;
using TypedQueueExecution;
using TypedQueueExecution.Core;
using TypedQueueExecution.Core.Enums;
using TypedQueueExecution.Core.Queue;

namespace NUnitTest
{
    [TestFixture]
    public class Priority
    {
        #region Поля

        private QueueExecution<NodePriority> _queue;
        private ManualResetEvent _event;

        private volatile bool _isHigh;
        private volatile bool _isNormal;
        private volatile bool _isLow;

        private bool _result;

        #endregion

        #region Методы

        [SetUp]
        public void Setup()
        {
            _queue = new QueueExecution<NodePriority>();
            _event = new ManualResetEvent(false);
            
            _result = false;
            _isHigh = false;
            _isNormal = false;
            _isLow = false;
        }

        [Test]
        [Order(0)]
        public void Check()
        {
            _queue.AddPerformer(0, new Performer<NodePriority>(3, new QueueNodePriority<NodePriority>()));

            _queue.AddInPerformer(0, new NodePriority(EPriority.High, 1, ActionHigh));
            _queue.AddInPerformer(0, new NodePriority(EPriority.Normal, 2, ActionNormal));
            _queue.AddInPerformer(0, new NodePriority(EPriority.Low, 3, ActionLow));

            _event.WaitOne();

            Assert.IsFalse(!_result);
        }

        private void ActionHigh(int value)
        {
            if (value != 1)
                Stop();

            _isHigh = true;
            if (_isNormal || _isLow) Stop();
        }

        private void ActionNormal(int value)
        {
            if (value != 2)
                Stop();

            _isNormal = true;
            if (!_isHigh || _isLow) Stop();
        }

        private void ActionLow(int value)
        {
            if (value != 3)
                Stop();

            _isLow = true;
            _result = _isHigh && _isNormal;

            Stop();
        }

        private void Stop()
        {
            _queue.Dispose();
            _event.Set();
        }

        #endregion
    }
}