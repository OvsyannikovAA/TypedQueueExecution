﻿using System.Threading;
using NUnit.Framework;
using NUnitTest.Core;
using TypedQueueExecution;
using TypedQueueExecution.Core;
using TypedQueueExecution.Core.Queue;

namespace NUnitTest
{
    [TestFixture]
    public class AddMany
    {
        #region Поля

        private QueueExecution<Node> _queue;
        private ManualResetEvent _event;

        private volatile int _count;
        private volatile int _index;
        private volatile bool _result;

        #endregion

        #region Методы

        [SetUp]
        public void Setup()
        {
            _queue = new QueueExecution<Node>();
            _event = new ManualResetEvent(false);

            _count = 1000;
            _index = 0;
            _result = false;
        }

        [Test]
        [Order(0)]
        [TestCase(10000000)]
        public void Check(int count)
        {
            _count = count;

            _queue.AddPerformer(0, new Performer<Node>(_count, new QueueNode<Node>()));

            for (var i = 0; i < _count; i++)
            {
                _queue.AddInPerformer(0, new Node(i, Action));
            }

            _event.WaitOne();

            Assert.IsFalse(!_result);
        }

        private void Action(int value)
        {
            if (_index != value || _index > _count)
            {
                _result = false;
                Stop();
            } else if (_index == _count - 1)
            {
                _result = true;
                Stop();
            }
            else
            {
                _index++;
            }
        }

        private void Stop()
        {
            _queue.Dispose();
            _event.Set();
        }

        #endregion
    }
}