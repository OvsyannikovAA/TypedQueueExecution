﻿using System;
using System.Threading;
using NUnit.Framework;
using NUnitTest.Core;
using TypedQueueExecution;
using TypedQueueExecution.Core;
using TypedQueueExecution.Core.Queue;

namespace NUnitTest
{
    [TestFixture]
    public class CheckLength
    {
        #region Поля

        private QueueExecution<Node> _queue;

        #endregion

        #region Методы

        [SetUp]
        public void Setup()
        {
            _queue = new QueueExecution<Node>();
        }

        [Test]
        [Order(0)]
        public void Check()
        {
            _queue.AddPerformer(0, new Performer<Node>(1, new QueueNode<Node>()));

            _queue.AddInPerformer(0, new Node(1, i => { Thread.Sleep(2000); }));
            var result = _queue.AddInPerformer(0, new Node(2, i => { Thread.Sleep(2000); }));

            Assert.IsFalse(result);
        }

        #endregion
    }
}